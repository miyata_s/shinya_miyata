package beans;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import controller.TopServletTest;

@RunWith(Suite.class)
@SuiteClasses({MessageTest.class,TopServletTest.class})
public class AllTests {

}
