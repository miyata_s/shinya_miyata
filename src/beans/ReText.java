package beans;


import java.io.Serializable;
import java.sql.Date;

public class ReText implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int userid;
    private int messageid;
    private String name;
    private String reply;
    private Date created_date;
    private Date updated_date;



    public int getId() {
        return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    public int getUserId() {
        return userid;
	}
	public void setUserId(int userid) {
		this.userid = userid;
	}
	public int getMessageid() {
        return messageid;
	}
	public void setMessageid(int messageid) {
		this.messageid = messageid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}

