package beans;

import java.io.Serializable;
import java.sql.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginid;
    private int userid;
    private String name;
    private String subject;
    private String category;
    private String text;
    private Date created_date;
    private Date updated_date;


    public int getId() {
        return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    public String getLoginId() {
        return loginid;
	}
	public void setLoginId(String loginid) {
		this.loginid = loginid;
	}
    public int getUserId() {
        return userid;
	}
	public void setUserId(int userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}
