package beans;


import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;


    private int id;
    private String loginid;
    private int userid;
    private String name;
    private String subject;
    private String category;
    private String text;
    private Date created_date;


    public int getId() {
        return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    public String getLoginId() {
        return loginid;
	}
	public void setLoginId(String loginid) {
		this.loginid = loginid;
	}
	public int getUserId() {
	    return userid;
	}
	public void setUserId(int userid) {
	    this.userid = userid;
	}
	public String getName() {
        return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;

	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

}
