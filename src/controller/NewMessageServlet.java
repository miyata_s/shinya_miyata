package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

/**
 * Servlet implementation class NewMessageServlet
 */
@WebServlet(urlPatterns = { "/newMessages" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> errors = new ArrayList<String>();

        if (isValid(request, errors) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();

//            message.setId(Integer.parseInt(request.getParameter("id")));
            message.setLoginId(user.getLoginId());
            message.setName(user.getName());
            message.setSubject(request.getParameter("subject"));
            message.setCategory(request.getParameter("category"));
            message.setText(request.getParameter("text"));

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", errors);
            response.sendRedirect("top");
        }
	}


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String subject = request.getParameter("subject");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

        HttpSession session = request.getSession();
        session.setAttribute("errorMessages", messages);


        if (StringUtils.isEmpty(subject)){
            messages.add("件名を入力してください");
        }
        if (30 < subject.length()){
            messages.add("30文字以下で入力してください");
        }
        if (StringUtils.isEmpty(category)){
            messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()){
            messages.add("10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(text)){
            messages.add("本文を入力してください");
        }
        if (1000 < text.length()){
            messages.add("1000文字以下で入力してください");
        }
        if (messages.size() == 0){
            return true;

    } else {
            return false;

        }
    }
}
