package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.ReText;
import beans.User;
import service.ReTextService;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet(urlPatterns = {"/reText"})
public class ReTextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	HttpSession session = request.getSession();

		List<String> errors = new ArrayList<String>();

		ReText reText = new ReText();

		if (isValid(request, errors) == true) {

			User user = (User) session.getAttribute("loginUser");

			reText.setId(user.getId());
			reText.setMessageid(Integer.parseInt(request.getParameter("messageid")));
			reText.setName(user.getName());
			reText.setReply(request.getParameter("reply"));

			new ReTextService().register(reText);
			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages", errors);
			response.sendRedirect("./");
		}
	}


	private boolean isValid(HttpServletRequest request, List<String> reTexts) {

        String reply = request.getParameter("reply");

        HttpSession session = request.getSession();
        session.setAttribute("errorMessages", reTexts);


        if (StringUtils.isEmpty(reply)){
        	reTexts.add("本文を入力してください");
        }
        if (500 < reply.length()){
        	reTexts.add("500文字以下で入力してください");

        }
        if (reTexts.size() == 0){
            return true;

    } else {
            return false;
	}
	}
}
