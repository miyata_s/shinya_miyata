package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

/**
 * Servlet implementation class EditUserServlet
 */
@WebServlet("/editUser")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");

		User ediUser = new UserService().getUser(loginUser.getId());
		request.setAttribute("ediUser", ediUser);

		request.getRequestDispatcher("editUser.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<String> errors = new ArrayList<String>();

		User editUser = getEditUser(request);

		if (isValid(request, errors) == true) {

			try {
                new UserService().update(editUser);

            } catch (NoRowsUpdatedRuntimeException e) {
            	errors.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", errors);


                session.setAttribute("errorMessages", errors);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("editUser").forward(request, response);
                return;
            }

	            session.setAttribute("loginUser", errors);
				response.sendRedirect("./");

	    	} else {
				session.setAttribute("errorMessages", errors);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("editUser.jsp").forward(request, response);
		}
	}


	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

        User editUser = new User();

//        User user = (User) request.getSession().getAttribute("loginUser");

//        editUser.setId(user.getId());
    	editUser.setLoginId(request.getParameter("loginid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(request.getParameter("branch"));
		editUser.setPosition(request.getParameter("position"));
        return editUser;
    }


	private boolean isValid(HttpServletRequest request, List<String> errors) {
		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String confpassword = request.getParameter("confpassword");
		String name = request.getParameter("name");
		String branch = request.getParameter("branch");
		String position = request.getParameter("position");


			if (StringUtils.isEmpty(loginid) == true) {
				errors.add("ログインIDを入力してください");
			}
			if (loginid.matches("^[0-9a-zA-Z]{6,20}$")) {
			}else{
				errors.add("ログインIDは半角英数字6文字以上20文字以下");
			}
	        if (StringUtils.isEmpty(password) == true) {
	        	errors.add("パスワードを入力してください");
		    }
	        if (password.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$")) {
			}else{
				errors.add("パスワードは記号を含む半角英数字6文字以上20文字以下");
			}
		    if (StringUtils.isEmpty(confpassword) == true) {
		    	errors.add("確認用パスワードを入力してください");
		    }
			if(!password.equals(confpassword)){
				errors.add("パスワードが一致しません");
			}
	        if (StringUtils.isEmpty(name) == true) {
	        	errors.add("名前を入力してください");
	        }
	        if (StringUtils.isEmpty(branch) == true) {
	        	errors.add("支店名を入力してください");
	        }
	        if (StringUtils.isEmpty(position) == true) {
	        	errors.add("部署・役職を入力してください");
	        }
	        if (errors.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
	}
}
