package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	     request.getRequestDispatcher("signup.jsp").forward(request, response);
	    }

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        List<String> messages = new ArrayList<String>();
	        HttpSession session = request.getSession(); //セッションよりログインユーザーの情報を取得

	        if (isValid(request, messages) == true) {

	            User user = new User();

	            user.setLoginId(request.getParameter("loginid"));
	            user.setPassword(request.getParameter("password"));
	            user.setName(request.getParameter("name"));
	            user.setBranch(request.getParameter("branch"));
	            user.setPosition(request.getParameter("position"));

	            new UserService().register(user);


       			 response.sendRedirect("settings");

	        } else {
	            session.setAttribute("errorMessages", messages);
	            response.sendRedirect("signup");
	        }
	    }

		private boolean isValid(HttpServletRequest request, List<String> messages) {

			String loginid = request.getParameter("loginid");
			String password = request.getParameter("password");
	        String confpassword = request.getParameter("confpassword");
			String name = request.getParameter("name");
			String branch = request.getParameter("branch");
			String position = request.getParameter("position");

			if (StringUtils.isEmpty(loginid) == true) {
			    messages.add("ログインIDを入力してください");
			}
			if (loginid.matches("^[0-9a-zA-Z]{6,20}$")) {
			}else{
			    messages.add("ログインIDは半角英数字6文字以上20文字以下");
			}
	        if (StringUtils.isEmpty(password) == true) {
		        messages.add("パスワードを入力してください");
		    }
	        if (password.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$")) {
			}else{
			    messages.add("パスワードは記号を含む半角英数字6文字以上20文字以下");
			}
		    if (StringUtils.isEmpty(confpassword) == true) {
			    messages.add("確認用パスワードを入力してください");
		    }
//		    String pass =  request.getParameter("password");
//           String cpass = request.getParameter("confpassword");
//			if(password != confpassword){
//				messages.add("パスワードが一致しません");
//			}
	        if (StringUtils.isEmpty(name) == true) {
	            messages.add("名前を入力してください");
	        }
	        if (StringUtils.isEmpty(branch) == true) {
	            messages.add("支店名を入力してください");
	        }
	        if (StringUtils.isEmpty(position) == true) {
	            messages.add("部署・役職を入力してください");
	        }
	        if (messages.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}