package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ReText;
import beans.User;
import beans.UserMessage;
import service.MessageService;
import service.ReTextService;

/**
 * Servlet implementation class TopServret
 */
@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 User user = (User) request.getSession().getAttribute("loginUser");

         boolean isShowMessageForm;
         if (user != null) {
             isShowMessageForm = true;
         } else {
             isShowMessageForm = false;
         }

         List<UserMessage> messages = new MessageService().getMessage();
         List<ReText> retexts = new ReTextService().getReText();


         request.setAttribute("messages", messages);
         request.setAttribute("retexts", retexts);
         request.setAttribute("isShowMessageForm", isShowMessageForm);

         request.getRequestDispatcher("top.jsp").forward(request, response);
    }



}

