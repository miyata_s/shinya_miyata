package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<User> users = new UserService().getUsers();
		request.setAttribute("users", users);
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<String> errors = new ArrayList<String>();

		User user = new User();

		if (isValid(request, errors) == true) {

			user.setLoginId(request.getParameter("loginid"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch(request.getParameter("branch"));
			user.setPosition(request.getParameter("position"));

			new UserService().register(user);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", errors);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> users) {
		String name = request.getParameter("name");

		HttpSession session = request.getSession();
		session.setAttribute("errorMessages", users);

		if (StringUtils.isEmpty(name)) {
			users.add("登録者はいません");
		}
		if (users.size() == 0) {
			return true;

		} else {
			return false;
		}
	}
}
