package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReText;
import beans.User;
import service.ReTextService;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

        List<String> errors = new ArrayList<String>();

		 HttpSession session = request.getSession() ;

   	 User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        request.setAttribute("errors", errors);
 		session.setAttribute("isShowMessageForm", isShowMessageForm);

 		response.sendRedirect("./");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		 HttpSession session = request.getSession() ;

	    List<ReText> delete  = new ReTextService().getDeleReplay
	    		(Integer.parseInt(request.getParameter("delete")));

		    request.setAttribute("delete", delete);
		    session.setAttribute("delete", delete);

		    response.sendRedirect("./");

	}

}
