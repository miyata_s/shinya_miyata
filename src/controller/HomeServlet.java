package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReText;
import beans.User;
import beans.UserMessage;
import service.MessageService;
import service.ReTextService;

/**
 * Servlet implementation class Home
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	   @Override
		    protected void doGet(HttpServletRequest request,
		    		HttpServletResponse response) throws IOException, ServletException {

		         List<String> errors = new ArrayList<String>();

		 		 HttpSession session = request.getSession() ;

		    	 User user = (User) request.getSession().getAttribute("loginUser");
		         boolean isShowMessageForm;
		         if (user != null) {
		             isShowMessageForm = true;
		         } else {
		             isShowMessageForm = false;
		         }

		         List<UserMessage> messages = new MessageService().getMessage();

		         List<UserMessage> category = new MessageService()
		        		 .getCategoryMessage(request.getParameter("category"));

		         List<ReText> reTexts = new ReTextService().getReText();

		 		 List<UserMessage> created_date = new MessageService()
		 				 .getCreated_date(request.getParameter("created_date"));


		        request.setAttribute("errors", errors);
		 		session.setAttribute("messages", messages);
		 		session.setAttribute("category", category);
		 		session.setAttribute("reTexts", reTexts);
		 		session.setAttribute("created_date", created_date);
		 		session.setAttribute("isShowMessageForm", isShowMessageForm);

		 		response.sendRedirect("home.jsp");
		    }


	   protected void doPost(HttpServletRequest request,
		            HttpServletResponse response) throws IOException, ServletException {

				HttpSession session = request.getSession();

				String year = request.getParameter("year");
				String month = request.getParameter("month");
				String days = request.getParameter("days");

				List<UserMessage> created_date = new MessageService()
						 .getCreated_date(year + "-" + month + "-" + days);

					request.setAttribute("created_date", created_date);
				    session.setAttribute("created_date", created_date);


			    List<UserMessage> category = new MessageService()
						 .getCategoryMessage(request.getParameter("category"));

					request.setAttribute("category", category);
				    session.setAttribute("category", category);


		        response.sendRedirect("home.jsp");
	   }
}
