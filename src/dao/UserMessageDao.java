package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.loginid as loginid, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.name as name, ");
            sql.append("messages.category as category, ");
            sql.append("messages.text as text, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	String loginid = rs.getString("loginid");
            	int userid = rs.getInt("user_id");
            	String category = rs.getString("category");
                String name = rs.getString("name");
                String text = rs.getString("text");
                Timestamp created_date = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setLoginId(loginid);
                message.setUserId(userid);
                message.setCategory(category);
                message.setName(name);
                message.setText(text);
                message.setCreated_date(created_date);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
