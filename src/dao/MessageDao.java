package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import exception.SQLRuntimeException;

public class MessageDao {

	 public void insert(Connection connection, Message message) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO messages ( ");
	            sql.append("id");
	            sql.append(", loginid");
	            sql.append(", user_id");
	            sql.append(", name");
	            sql.append(", subject");
	            sql.append(", category");
	            sql.append(", text");
	            sql.append(", created_date");
	            sql.append(", updated_date");
	            sql.append(") VALUES (");
	            sql.append("?"); // id
	            sql.append(", ?");//loginid
	            sql.append(", ?");//userid
	            sql.append(", ?");// name
	            sql.append(", ?");//subject
	            sql.append(", ?");//category
	            sql.append(", ?");// text
	            sql.append(", CURRENT_TIMESTAMP"); // created_date
	            sql.append(", CURRENT_TIMESTAMP"); // updated_date
	            sql.append(")");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setInt(1, message.getId());
	            ps.setString(2, message.getLoginId());
	            ps.setInt(3, message.getUserId());
	            ps.setString(4, message.getName());
	            ps.setString(5, message.getSubject());
	            ps.setString(6, message.getCategory());
	            ps.setString(7, message.getText());

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }


	     public List<UserMessage> getUserMessages(Connection connection, int num) {

	         PreparedStatement ps = null;
	         try {
	             StringBuilder sql = new StringBuilder();
	             sql.append("SELECT * FROM messages ");
	             sql.append("ORDER BY created_date DESC ");

	             ps = connection.prepareStatement(sql.toString());

	             ResultSet rs = ps.executeQuery();
	             List<UserMessage> ret = toUserMessageList(rs);
	             return ret;
	         } catch (SQLException e) {
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(ps);
	         }
	     }


	 	private List<UserMessage> toUserMessageList(ResultSet rs)
	             throws SQLException {

	         List<UserMessage> ret = new ArrayList<UserMessage>();
	         try {
	             while (rs.next()) {
	            	 int id = rs.getInt("id");
	            	 String loginid = rs.getString("loginid");
	            	 int userid = rs.getInt("user_id");
	                 String name = rs.getString("name");
	                 String subject = rs.getString("subject");
	 	             String category = rs.getString("category");
	                 String text = rs.getString("text");
	                 Timestamp created_date = rs.getTimestamp("created_date");

	                 UserMessage message = new UserMessage();

	                 message.setId(id);
	                 message.setLoginId(loginid);
	                 message.setUserId(userid);
	                 message.setName(name);
	                 message.setSubject(subject);
	                 message.setCategory(category);
	                 message.setText(text);
	                 message.setCreated_date(created_date);

	                 ret.add(message);
	             }
	             return ret;
	         } finally {
	             close(rs);
	         }
	 	}

	    public List<UserMessage> getCategoryMessages(Connection connection, String category) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * FROM messages WHERE category like ? ");
	            ps = connection.prepareStatement(sql.toString());
	    		ps.setString(1, "%" + category + "%");

	            ResultSet rs = ps.executeQuery();
	            List<UserMessage> ret = toUserMessageList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    public List<UserMessage> getCreated_date(Connection connection, String created_date) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * FROM messages WHERE created_date like ?");
	            ps = connection.prepareStatement(sql.toString());
	    		ps.setString(1,"%" + created_date + "%");

	            ResultSet rs = ps.executeQuery();
	            List<UserMessage> ret = toUserMessageList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }



}








