package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("id");
            sql.append(", loginid");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch");
            sql.append(", position");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?");   // id
            sql.append(", ?"); // loginid
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // position
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user.getId());
            ps.setString(2, user.getLoginId());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getName());
            ps.setString(5, user.getBranch());
            ps.setString(6, user.getPosition());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getUsers(Connection connection) {
    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users";
    		ps = connection.prepareStatement(sql);

    		ResultSet rs = ps.executeQuery();
    		return toUserList(rs);
    	} catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
    	}
    }

	public User getUser(Connection connection, String loginid, String password) {
		PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE loginid = ? AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, loginid);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	        	int id = rs.getInt("id");
	        	String loginid = rs.getString("loginid");
	            String password = rs.getString("password");
	            String name = rs.getString("name");
	            String branch = rs.getString("branch");
	            String position = rs.getString("position");
	            Timestamp created_date = rs.getTimestamp("created_date");
	            Timestamp updated_date = rs.getTimestamp("updated_date");

	            User user = new User();

	            user.setId(id);
	            user.setLoginId(loginid);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranch(branch);
	            user.setPosition(position);
	            user.setCreated_date(created_date);
	            user.setUpdatedDate(updated_date);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE loginid = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	 public void update(Connection connection, User user) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append(" id = ?");
	            sql.append(", loginid = ?");
	            sql.append(", password = ?");
	            sql.append(", confpassword = ?");
	            sql.append(", name = ?");
	            sql.append(", branch = ?");
	            sql.append(", position = ?");
	            sql.append(", updated_date = CURRENT_TIMESTAMP");
	            sql.append(" WHERE");

	            ps = connection.prepareStatement(sql.toString());
	            ps.setInt(1, user.getId());
	            ps.setString(2, user.getLoginId());
	            ps.setString(3, user.getPassword());
	            ps.setString(4, user.getConfPassword());
	            ps.setString(5, user.getName());
	            ps.setString(6, user.getBranch());
	            ps.setString(7, user.getPosition());


	            int count = ps.executeUpdate();
	            if (count == 0) {
	                throw new NoRowsUpdatedRuntimeException();
	            }
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }
	}

