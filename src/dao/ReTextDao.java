package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.ReText;
import exception.SQLRuntimeException;

public class ReTextDao {

  public void insert(Connection connection, ReText reText) {

    PreparedStatement ps = null;
    try {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO reTexts ( ");
        sql.append("id");
        sql.append(", user_id");
        sql.append(", messageid");
        sql.append(", name");
        sql.append(", reply");
        sql.append(", created_date");
        sql.append(", updated_date");
        sql.append(") VALUES (");
        sql.append("?"); // id
        sql.append(", ?");//userid
        sql.append(", ?");// name
        sql.append(", ?");// name
        sql.append(", ?");// reTaxt
        sql.append(", CURRENT_TIMESTAMP"); // created_date
        sql.append(", CURRENT_TIMESTAMP"); // updated_date
        sql.append(")");

        ps = connection.prepareStatement(sql.toString());


        ps.setInt(1, reText.getId());
        ps.setInt(2, reText.getUserId());
        ps.setInt(3, reText.getMessageid());
        ps.setString(4, reText.getName());
        ps.setString(5, reText.getReply());

        ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}


 public List<ReText> getReText(Connection connection) {

     PreparedStatement ps = null;
     try {
         StringBuilder sql = new StringBuilder();
         sql.append("SELECT * FROM reTexts ");
         sql.append("ORDER BY created_date DESC ");

         ps = connection.prepareStatement(sql.toString());

         ResultSet rs = ps.executeQuery();
         List<ReText> ret = toReTextList(rs);
         return ret;

     } catch (SQLException e) {
         throw new SQLRuntimeException(e);
     } finally {
         close(ps);
     }
 }

 public List<ReText> toReTextList(ResultSet rs) throws SQLException {

     List<ReText> ret = new ArrayList<ReText>();
     try {
         while (rs.next()) {
        	 int id = rs.getInt("id");
             int userid = rs.getInt("user_id");
             int messageid = rs.getInt("messageid");
             String name = rs.getString("name");
             String reply = rs.getString("reply");
             Date created_date = rs.getDate("created_date");

             ReText reText = new ReText();

             reText.setId(id);
             reText.setUserId(userid);
             reText.setMessageid(messageid);
             reText.setName(name);
             reText.setReply(reply);
             reText.setCreated_date(created_date);

             ret.add(reText);
         }
         return ret;
     } finally {
     }
 }



 public  List<ReText> getDeleReplay(Connection connection,int id) {

     PreparedStatement ps = null;
     try {
    	 StringBuilder sql = new StringBuilder();
    	 sql.append("DELETE FROM reTexts WHERE id=?");

         ps = connection.prepareStatement(sql.toString());
    	 ps.setInt(1,id);

         ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	return null;
    }
}
