package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;


public class MessageService {

		public void register(Message message) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            MessageDao messageDao = new MessageDao();
	            messageDao.insert(connection, message);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }


		public List<UserMessage> getCategoryMessage(String category) {

		    Connection connection = null;
		    try {
		        connection = getConnection();

		        MessageDao getMessageDao = new MessageDao();
		        List<UserMessage> ret = getMessageDao.getCategoryMessages(connection, category);

		        commit(connection);

		        return ret;
			    } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
			    } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
		}

		    public List<UserMessage> getCreated_date(String created_date) {

			    Connection connection = null;
			    try {
			        connection = getConnection();

			        MessageDao getMessageDao = new MessageDao();
			        List<UserMessage> ret = getMessageDao.getCreated_date(connection, created_date);

			        commit(connection);

			        return ret;
				    } catch (RuntimeException e) {
		            rollback(connection);
		            throw e;
				    } catch (Error e) {
		            rollback(connection);
		            throw e;
		        } finally {
		            close(connection);
		        }
	    }


		private static final int LIMIT_NUM = 10000;

		public List<UserMessage> getMessage() {

		    Connection connection = null;
		    try {
		        connection = getConnection();

		        MessageDao getMessageDao = new MessageDao();
		        List<UserMessage> ret = getMessageDao.getUserMessages(connection, LIMIT_NUM);

		        commit(connection);

		        return ret;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
		}
}

