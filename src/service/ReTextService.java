package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.ReText;
import dao.ReTextDao;


public class ReTextService {

	public void register(ReText reText) {

        Connection connection = null;
        try {
            connection = getConnection();

            ReTextDao reTextDao = new ReTextDao();
            reTextDao.insert(connection, reText);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<ReText> getReText(){
		Connection conn = null;
    	try {
    		conn = getConnection();
    		ReTextDao dao = new ReTextDao();

    		return dao.getReText(conn);
        } finally {
            close(conn);
        }
	}

    public List<ReText> getDeleReplay(int id) {

    	Connection connection = null;

	    try {
	        connection = getConnection();

	        ReTextDao getReTextDao = new ReTextDao();
	        List<ReText> ReText = getReTextDao.getDeleReplay(connection,id);


	        commit(connection);

	        return ReText;
		    } catch (RuntimeException e) {
            rollback(connection);
            throw e;
		    } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }




}