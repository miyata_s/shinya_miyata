/**
 *
 */
package beans;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author admin
 *
 */
public class MessageTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	/**
	 * {@link beans.Message#getId()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetId() {
		Message message = new Message();
		assertEquals(10000000,message.getId());
	}

	/**
	 * {@link beans.Message#setId(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetId() {
		System.out.println("testSetId");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getLoginId()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetLoginId() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setLoginId(java.lang.String)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetLoginId() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getUserId()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetUserId() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setUserId(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetUserId() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getName()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetName() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setName(java.lang.String)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetName() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getSubject()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetSubject() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setSubject(java.lang.String)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetSubject() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getCategory()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetCategory() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setCategory(java.lang.String)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetCategory() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getText()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetText() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setText(java.lang.String)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetText() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getCreatedDate()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetCreatedDate() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setCreatedDate(java.sql.Date)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetCreatedDate() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#getUpdatedDate()} のためのテスト・メソッド。
	 */
	@Test
	public void testGetUpdatedDate() {
		System.out.println("");
		fail("まだ実装されていません");
	}

	/**
	 * {@link beans.Message#setUpdatedDate(java.sql.Date)} のためのテスト・メソッド。
	 */
	@Test
	public void testSetUpdatedDate() {
		System.out.println("");
		fail("まだ実装されていません");
	}

}
