<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<body>

       <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <div class="header">
                <a href="./">ホーム</a>
            </div>


          <form action="editUser" method="post">

                <input type="hidden" name="id" value="${editUser.id}" id="id" />

                <label for="password">新ログインID</label>
                <input name="loginid" value="${editUser.loginid}" id="loginid"/><br />

                <label for="password">新パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                 <label for="password">パスワード(確認用)</label>
                <input name="confpassword" type="password" id="password"/> <br />

                <label for="name">新名称</label>
                <input name="name" value="${editUser.name}" id="name"/><br />

                <label for="branch">支店</label>
                <input name="branch" value="${editUser.branch}" id="branch"/> <br />

                <label for="position">新所属・役職</label>
                <input name="position" value="${editUser.position}" id="position"/> <br />

                <input type="submit" value="変更" /> <br />
                <a href="settings">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>


</body>
</html>