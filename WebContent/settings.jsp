<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>$ユーザー管理</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <div class="header">
         	    <a href="./">ホーム</a>
                <a href="signup">ユーザー新規登録</a>
                <a href="editUser">ユーザー編集</a>
            </div>


             <div class="settings">
             <c:forEach items="${users}" var="user">
                   <hr><div class="loginid">ログインID:<c:out value="${user.loginId}" /></div>
                   <div class="name">名前:<c:out value="${user.name}" /></div>
                   <div class="branch">支店:<c:out value="${user.branch}" /></div>
                   <div class="position">部署・役職:<c:out value="${user.position}" /></div>
                   <div class="created_date"><fmt:formatDate value="${user.created_date}" pattern="y年M月d日 HH:mm:ss" /></div>
             </c:forEach><hr>
             </div>


             <a href="./">戻る</a>

            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>