<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
       <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ホーム画面</title>
       </head>
    <body>
        <div class="main-contents">

           <c:if test="${ not empty errorMessages }">
              <div class="errorMessages">
                   <ul>
                       <c:forEach items="${errorMessages}" var="message">
                           <li><c:out value="${message}" />
                       </c:forEach>
                   </ul>
               </div>
               <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <div class="header">
                <a href="./">ホーム</a>
                <a href="top">新規投稿画面</a>
                <a href="settings">ユーザー管理画面</a>
                <a href="logout">ログアウト</a>
            </div>

             <c:if test="${ empty loginUser }">
  		  	    <div class="profile">
     		      <a href="login">ログイン</a>
           		  </div>
   		  	</c:if>


            <c:if test="${ not empty loginUser }">
           		  <div class="profile">
           		  <a><B><br>＜ログイン中のユーザー＞</B></a>
            	     <div class="name">名前：<c:out value="${loginUser.name}" /></div>
            	     <div class="branch">支店名:<c:out value="${loginUser.branch}" /></div><br />
           		  </div>
   		  	</c:if>


            <form action="./" method="post">
                  <label> カテゴリー:
                  <input type="text" name="category" size="20" maxlength="50"></label>
                  <input type="submit" value="検索">
            </form>


            <form action="./" method="post">
                <br>投稿日:
                <select name="year">
               		<option value=""></option>
            		<option value="2017">2017</option>
            	    <option value="2018">2018</option>
            	    <option value="2019">2019</option>
              	</select>年

                <select name="month">
                   <option value=""></option>
                   <option value="1">1</option>
                   <option value="2">2</option>
                   <option value="3">3</option>
                   <option value="4">4</option>
                   <option value="5">5</option>
                   <option value="6">6</option>
                   <option value="7">7</option>
                   <option value="8">8</option>
                   <option value="9">9</option>
                   <option value="10">10</option>
                   <option value="11">11</option>
                   <option value="12">12</option>
                </select>月


                <select name="days">
                   <option value=""></option>
                   <option value="1">1</option>
                   <option value="2">2</option>
                   <option value="3">3</option>
                   <option value="4">4</option>
                   <option value="5">5</option>
                   <option value="6">6</option>
                   <option value="7">7</option>
                   <option value="8">8</option>
                   <option value="9">9</option>
                   <option value="10">10</option>
                   <option value="11">11</option>
                   <option value="12">12</option>
                   <option value="13">13</option>
                   <option value="14">14</option>
                   <option value="15">15</option>
                   <option value="16">16</option>
                   <option value="17">17</option>
                   <option value="18">18</option>
                   <option value="19">19</option>
                   <option value="20">20</option>
                   <option value="21">21</option>
                   <option value="22">22</option>
                   <option value="23">23</option>
                   <option value="24">24</option>
                   <option value="25">25</option>
                   <option value="26">26</option>
                   <option value="27">27</option>
                   <option value="28">28</option>
                   <option value="29">29</option>
                   <option value="30">30</option>
                   <option value="31">31</option>
                </select>日
                   <input type="submit" value="検索"><hr>
           	</form>


		<c:if test ="${ empty created_date && empty category }">
                 <c:forEach items="${messages}" var="message">
                    <div class="messages">
                      <hr><div class="id"><U>投稿no.<c:out value="${message.id}" /></U></div>
                      <div class="name">名前:<c:out value="${message.name}" /></div>
                      <div class="subject">件名:<c:out value="${message.subject}" /></div>
                      <div class="category">カテゴリー:<c:out value="${message.category}" /></div>
                      <div class="text">本文:<c:out value="${message.text}" /></div>
                      <div class="created_date"><fmt:formatDate value="${message.created_date}" pattern="y年M月d日 HH:mm:ss" /></div><br />
                    </div>

                  <c:forEach items="${reTexts}" var="reText">
                  <c:if test="${ reText.messageid == message.id }">
                  <div class="reText">
                         <div class="reply">＞返信履歴:<c:out value="${reText.reply}" /></div>
                         <div class="name">＞返信者:<c:out value="${reText.name}" /></div>
                         <div class="created_date"><fmt:formatDate value="${reText.created_date}" pattern="y年M月d日 HH:mm:ss" /></div>

					<form action="delete" method="post">
                         <input type="hidden" name="delete" value="${reText.messageid }">
                         <input type="submit" value="削除" >
                    </form>
                  </div>
                  </c:if>
                 </c:forEach>

               		<form action="reText" method="post">
	                     <input type="hidden" name="messageid" value="${message.id}">
	                     newコメント:
	                     <textarea name="reply" cols="35" rows="2" class="reText_box"></textarea>
	                     <br><input type="submit" value="送信"><hr />
           		  	</form>
                 </c:forEach>
           </c:if>
            <c:remove var="delete" scope="session"/>


    	<c:if test ="${not empty created_date}">
                 <c:forEach items="${created_date}" var="created_date">
                      <hr><div class="id"><U>投稿no.<c:out value="${created_date.id}" /></U></div>
                      <div class="name">名前:<c:out value="${created_date.name}" /></div>
                      <div class="subject">件名:<c:out value="${created_date.subject}" /></div>
                      <div class="category">カテゴリー:<c:out value="${created_date.category}" /></div>
                      <div class="text">本文:<c:out value="${created_date.text}" /></div>
                      <div class="created_date"><fmt:formatDate value="${created_date.created_date}" pattern="y年M月d日 HH:mm:ss" /></div><br />

                  <c:forEach items="${reTexts}" var="reText">
                  	<c:if test="${ reText.messageid == created_date.id }">
                	  <div class="reText">
                         <div class="reply">＞返信履歴:<c:out value="${reText.reply}" /></div>
                         <div class="name">＞返信者:<c:out value="${reText.name}" /></div>

                         <form action="delete" method="post">
                         <input type="hidden" name="delete" value="${reText.messageid }">
                         <input type="submit" value="削除" >
                    </form>
                 	 </div>
              		</c:if>
         		  </c:forEach>

       			<form action="reText" method="post">
                 	  <input type="hidden" name="messageid" value="${created_date.id}">
                   		newコメント:
                   		<textarea name="reply" cols="35" rows="2" class="reText_box"></textarea>
                   		<br><input type="submit" value="送信"><hr />
           		</form>
                </c:forEach>
       	  	</c:if>
       	  	 <c:remove var="delete" scope="session"/>


			<c:if test ="${not empty category}">
                 <c:forEach items="${category}" var="category">
                      <hr><div class="id"><U>投稿no.<c:out value="${category.id}" /></U></div>
                      <div class="name">名前:<c:out value="${category.name}" /></div>
                      <div class="subject">件名:<c:out value="${category.subject}" /></div>
                      <div class="category">カテゴリー:<c:out value="${category.category}" /></div>
                      <div class="text">本文:<c:out value="${category.text}" /></div>
                      <div class="created_date"><fmt:formatDate value="${category.created_date}" pattern="y年M月d日 HH:mm:ss" /></div><br />


                  <c:forEach items="${reTexts}" var="reText">
                  	<c:if test="${ reText.messageid == category.id }">
                	  <div class="reText">
                         <div class="reply">＞返信履歴:<c:out value="${reText.reply}" /></div>
                         <div class="name">＞返信者:<c:out value="${reText.name}" /></div>
                         <div class="created_date"><fmt:formatDate value="${reText.created_date}" pattern="y年M月d日 HH:mm:ss" /></div><br />

                         <form action="delete" method="post">
                         <input type="hidden" name="delete" value="${reText.messageid }">
                         <input type="submit" value="削除" >
                         </form>
                 	 </div>
              		</c:if>
         		  </c:forEach>

       			<form action="reText" method="post">
                 	  <input type="hidden" name="messageid" value="${category.id}">
                   		newコメント:
                   		<textarea name="reply" cols="35" rows="2" class="reText_box"></textarea>
                   		<br><input type="submit" value="送信"><hr />
           		</form>
                </c:forEach>
       	  	</c:if>
       	  	 <c:remove var="delete" scope="session"/>
            <br>
            <div class="copyright"> Copyright(c)YourName</div>
       </div>
     </body>
 </html>
