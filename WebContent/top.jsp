<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
       </head>
    <body>
        <div class="main-contents">

              <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
              </c:if>

           <div class="header">
             <c:if test="${ empty loginUser }">
                <a href="login">ログイン</a>
                <a href="signup">ユーザー登録</a>
             </c:if>
             <c:if test="${ not empty loginUser }">
                <a href="./">ホーム画面</a>

             </c:if>
            </div>


            <c:if test="${ not empty loginUser }">
             <div class="profile">
             <a><B><br>＜ログイン中のユーザー＞</B></a>
                 <div class="name">名前：<c:out value="${loginUser.name}" /></div>
                 <div class="branch">支店名:<c:out value="${loginUser.branch}" /></div><br />
             </div>
              </c:if>


             <div class="form-area">
              <c:if test="${ isShowMessageForm }">
                <form action="newMessages" method="post">
                 <input type="hidden" name="id" value="id">
                 件名:
                 <textarea name="subject" cols="32" rows="1" class="tweet-box2"></textarea><br />
                 カテゴリー:
                 <textarea name="category" cols="25" rows="1" class="tweet-box3"></textarea><br />
                 本文:<br />
                 <textarea name="text" cols="40" rows="5" class="tweet-box4"></textarea><br />
                 <input type="submit" value="新規投稿">（1000文字まで）<br />
                 <input type="reset" value="削除">
                </form>
              </c:if>
            </div>


             <div class="messages">
              <c:forEach items="${messages}" var="message">
                   <hr><div class="id"><U>投稿no.<c:out value="${message.id}" /></U></div>
                       <div class="name">名前:<c:out value="${message.name}" /></div>
                       <div class="subject">件名:<c:out value="${message.subject}" /></div>
                       <div class="category">件名:<c:out value="${message.category}" /></div>
                       <div class="text">本文:<c:out value="${message.text}" /></div>
                       <div class="created_date"><fmt:formatDate value="${message.created_date}" pattern="y年M月d日 HH:mm:ss" /></div>


                  <c:forEach items="${reTexts}" var="reText">
                  <c:if test="${ reText.messageid == message.id }">
                  <div class="reText">
                         <div class="reply">＞返信履歴:<c:out value="${reText.reply}" /></div>
                         <div class="name">＞返信者:<c:out value="${reText.name}" /></div>
                         <div class="created_date"><fmt:formatDate value="${reText.created_date}" pattern="y年M月d日 HH:mm:ss" /></div>
                         <input type="submit" name="delete" value="削除">
                         <input type="hidden" name="reText.messageid" value="${reText.messageid}">
                  </div>
                  </c:if>
                 </c:forEach>
                 </c:forEach>


              </div>

              <br>
              <div class="copyright"> Copyright(c)YourName</div>
         </div>
    </body>
</html>